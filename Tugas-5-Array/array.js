// NO 1
function range(start,finish) {
    if (!finish || !start){
        return -1;
    } else if (start < finish){
        var x = [];
        for (let index = start; index <= finish; index++) {
            x.push(index);
        }
        return x;
    } else if (start > finish){
        var x = []; var i = start;
        while (i >= finish){
            x.push(i);
            i--;
        }
        return x;
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// NO 2
function rangeWithStep(start,finish,step) {
    if (!finish || !start){
        return -1;
    } else if (start < finish){
        var x = [];
        for (let index = start; index <= finish; index+=step) {
            x.push(index);
        }
        return x;
    } else if (start > finish){
        var x = []; var i = start;
        while (i >= finish){
            x.push(i);
            i-=step;
        }
        return x;
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// NO 3
function sum(start,finish,step=1) {
    if (!start && !finish){
        return 0;
    }
    else if (!start){
        return 0;
    } else if (!finish){
        return start;
    } else if (start < finish){
        var x = 0;
        for (let index = start; index <= finish; index+=step) {
            x+=index;
        }
        return x;
    } else if (start > finish){
        var x = 0; var i = start;
        while (i >= finish){
            x+=i;
            i-=step;
        }
        return x;
    }
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// NO 4
function dataHandling(L) {
    for (let index = 0; index < L.length; index++) {
        console.log(
            `\nNomor ID:  `+ L[index][0] +
            `\nNama Lengkap: `+L[index][1] +
            `\nTTL: `+ L[index][2] +` `+ L[index][3] +
            `\nHobi: `+ L[index][4]
        );        
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input))
console.log(' ')


// NO 5
function balikKata(k) {
    var x = ""; var i = k.length-1;
        while (i >= 0){
            x+=k[i];
            i--;
        }
        return x;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log(' ')

// NO 6
function dataHandling2(L) {
    L.splice(2,1,"Provinsi Bandar Lampung");
    L.splice(4,1,"Pria");
    L.splice(5,0,"SMA Internasional Metro");
    console.log(L);

    var date = L[3].split("/")
    switch(parseInt(date[1])){
        case 1: {console.log('Januari '); break;}
        case 2: {console.log('Februari '); break;}
        case 3: {console.log('Maret '); break;}
        case 4: {console.log('April '); break;}
        case 5: {console.log('Mei '); break;}
        case 6: {console.log('Juni '); break;}
        case 7: {console.log('Juli '); break;}
        case 8: {console.log('Agustus '); break;}
        case 9: {console.log('September '); break;}
        case 10: {console.log('Oktober '); break;}
        case 11: {console.log('November '); break;}
        case 12: {console.log('Desember '); break;}
        default: {console.log('Bulan salah!'); break;}
    }
    
    var t = L[3].split('/').sort(function (value1, value2) { return value2 - value1 } );
    console.log(t);

    console.log(date.join("-"));

    var n = L[1].slice(0,15)
    console.log(n);

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 