// NO 1
const readBooks = require('./callback.js').default;
const books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];

const fulltime = 10000;
// callback dengan memanggil antrian buku selanjutnya
readBooks(fulltime, books[0], time => {
    readBooks(time, books[1], time => {
        readBooks(time, books[2], time => {
            console.log('Sisa waktu: ' + time);
        })
    })
});