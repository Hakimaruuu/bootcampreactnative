// NO 2
const readBooksPromise = require('./promise.js').default;
const books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];

const fulltime = 10000;
readBooksPromise(fulltime, books[0])
    .then(time => {
        readBooksPromise(time, books[1])
            .then(time => {
                readBooksPromise(time, books[2]);
            })
    })
    .catch(err => console.log('Error: ' + err));