import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Tugas17 from './Tugas/Tugas17/index';

// Import the functions you need from the SDKs you need
import firebase from 'firebase/app';
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCvIWCxue5hwzUIXHGNzU3FajlMoXECIJM",
  authDomain: "spun-video.firebaseapp.com",
  projectId: "spun-video",
  storageBucket: "spun-video.appspot.com",
  messagingSenderId: "214399590056",
  appId: "1:214399590056:web:d267824564a3f52bb82c9d",
  measurementId: "G-8QNZHTRESS"
};

// Initialize Firebase
let app;
if (!firebase.apps.length){
  app=  firebase.initializeApp(firebaseConfig)
}

export default function App() {
  return (
    <>
    <Tugas17/>
    </>
  );
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  }
})