import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Router from './Router/index'

export default function index() {
    return (
        <Router />
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        color: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
});