import { StatusBar } from 'expo-status-bar';
import React from 'react'
import {
    View,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Pressable,
} from 'react-native';

const {height,width} = Dimensions.get('window');

const colors = {
    primary: "#212c3a",
    secondary: "#3371a9",
}


export default function loginScreen() {
    const [username,onChangeUsername] = React.useState('Username');
    const [pass,onChangePass] = React.useState('Password');
    const [confpass,onChangeConfPass] = React.useState('Confirm Password');

    return (
        <>
        <StatusBar style='light' translucent={false} backgroundColor={'#51a5f2'}/>
        <View style={styles.container}>
            <View style={styles.imageView}>
                <Image source={{uri: 'https://media.istockphoto.com/vectors/home-icon-house-vector-cabin-illustration-vector-id1327874024?k=20&m=1327874024&s=612x612&w=0&h=6YoCel8DKHmotqKqAjgg_akEi6qFmbL7kacPs3zegrM='}} style={styles.imageIcon} />
            </View>
            <View style={styles.mainContent}>
                <Text style={styles.mainText}>
                    Register
                </Text>
                <TouchableOpacity style={styles.inputContent}>
                    <TextInput style={styles.inputText} value={username} placeholder="Username" onChangeText={onChangeUsername} />
                    <TextInput style={styles.inputText} value={pass} placeholder="Password" onChangeText={onChangePass} />
                    <TextInput style={styles.inputText} value={confpass} placeholder="Confirm Password" onChangeText={onChangeConfPass} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonContent}>
                    <Pressable style={styles.button2}>
                        <Text style={styles.registerText}>
                            LOGIN
                        </Text>
                    </Pressable>
                    <Pressable style={styles.button1}>
                        <Text style={styles.loginText}>
                            REGISTER
                        </Text>
                    </Pressable>
                </TouchableOpacity>
            </View>
        </View>
        </>
    );
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex: 1,
    },
    inputText:{
        height: 53,
        margin: height * 0.01,
        borderWidth: 1,
        fontSize: 18,
        padding: 12,
        borderRadius: 5,
        color: '#afafaf',
        borderColor: '#afafaf',
        
    },
    inputContent:{
        paddingTop: height * 0.01,
    },
    imageView:{
        paddingTop: height * 0.1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageIcon:{
        width: 220,
        height: 220,
        resizeMode: 'contain',
        borderRadius: 300,
    },
    mainText:{
        color: 'black',
        fontWeight: '600',
        fontSize:36,
        paddingLeft: width * 0.04,
    },
    mainContent:{
        marginTop: height * -0.01,
    },
    buttonContent:{
        height: height * 0.37,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button1:{
        height: 53,
        width: 168,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        margin: 11,
        elevation: 3,
        backgroundColor: '#51a5f2',
    },
    button2:{
        height: 53,
        width: 168,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        margin: 11,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'white',
        borderColor:'#51a5f2',
    },
    loginText:{
        fontSize: 18,
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: '#fff',
    },
    registerText:{
        fontSize: 18,
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: '#51a5f2'
    }
});
