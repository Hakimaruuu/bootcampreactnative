import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const data = [
    {
        id:1,
        title:'test',
        desc: 'helo',
    },
    {
        id:2,
        title:'test',
        desc: 'helo',
    },
    {
        id:3,
        title:'test',
        desc: 'helo',
    }
]
export default function Quiz3() {
    return (
        <View style={styles.container}>
            {
                data.map((item)=>{
                    return (
                        <Text>
                            {item.title}
                        </Text>
                    )
                })
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center'
    }
})