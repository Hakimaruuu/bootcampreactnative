import React, {useEffect,useState} from 'react'
import { View, Text } from 'react-native'

export default function quiz5() {
    const [Name, setName] = useState('initialState')

    useEffect(() => {
        setTimeout(()=>{
            setName('Pulu')
        }, 2000)
        return () => {
            setName('Adududu')
        }
    }, [])
    
    return (
        <View style={ {alignItems: 'center', justifyContent: 'center',  flex: 1,}}>
            <Text>{Name}</Text>
        </View>
    )
}
