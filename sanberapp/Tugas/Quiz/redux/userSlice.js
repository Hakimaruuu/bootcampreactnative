import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    email: '',
    token: ''
};

export const userSlice = createSlice({
    name:'counter',
    initialState,
    reducers:{
        login: (state, action)=>{
            state.email= action.payload.email;
            state.token= action.payload.token;
        },
        logout: (state)=>{
            state.email= null,
            state.token= null
        },
    },
});

export const {login,logout}=userSlice.actions;
export default userSlice.reducer;