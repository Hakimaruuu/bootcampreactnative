import React from 'react';
import MovieList from './MovieList';
import MovieForm from './MovieForm';

const MovieScreen = () => {
    return (
        <>
        <MovieList />
        <MovieForm />
        </>
    );
};

export default MovieScreen;