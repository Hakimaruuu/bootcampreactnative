import React from 'react'
import { View, Text, Button, StyleSheet, Pressable } from 'react-native'
import { decrement,increment, incrementByAmount } from './counterSlice'
import { useSelector,useDispatch } from 'react-redux'


export default function counterScreen() {
    const count = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()
    return (
        <View>
            <Button color="green" title="increment" onPress={()=>dispatch(increment())} />
            <Text>
                {count}
            </Text>
            <Button color="red" title="decrement" onPress={()=>dispatch(decrement())} />
            <Button color="yellow" title="incByValue" onPress={()=>dispatch(incrementByAmount({value:5}))} />
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    }
});