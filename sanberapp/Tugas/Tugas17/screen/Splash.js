import React, {useEffect} from 'react'
import { View, Image, Dimensions } from 'react-native'

const {height,width} = Dimensions.get('window');

export default function Splash({navigation}) {
    useEffect(() => {
        setTimeout(()=>{
            navigation.navigate('Welcome');
        }, 2500)
    }, [])

    return (
        <View>
            <Image source={require('../asset/splash.png')} style={{
                height: height,
                width: width,
            }} />
        </View>
    )
}
