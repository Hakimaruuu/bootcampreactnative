import { StatusBar } from 'expo-status-bar';
import React from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Pressable,
} from 'react-native';

const {height,width} = Dimensions.get('window');

const colors = {
    primary: '#51a5f2',
    secondary: '#013064',
    background: '#212c3a',
    black: '#212121'

};

export default function Welcome({navigation}) {
    return (
        <>
        <StatusBar style='light' translucent={false} backgroundColor={'#51a5f2'}/>
        <View style={styles.container}>
            <View style={styles.imageView}>
                <Image source={require('../asset/bgc.jpg')} style={styles.imageIcon} />
            </View>
            <View style={styles.mainContent}>
                <View style={styles.footer}>
                    <Text style={styles.mainText}>
                        Welcome!
                    </Text>
                    <TouchableOpacity style={styles.inputContent}>
                        <Pressable style={styles.button1} onPress={()=>navigation.navigate('Login')}>
                            <Text style={styles.loginText}>
                                Login
                            </Text>
                        </Pressable>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.inputContent}>
                        <Pressable style={styles.button2} onPress={()=>navigation.navigate('Register')}>
                            <Text style={styles.registerText}>
                                Sign Up
                            </Text>
                        </Pressable>
                    </TouchableOpacity>
                    
                </View>
            </View>
        </View>
        </>
    );
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: colors.background,
        flex: 1,
    },
    inputText:{
        height: 53,
        margin: height * 0.01,
        borderWidth: 1,
        fontSize: 18,
        padding: 12,
        color: '#afafaf',
        borderRadius: 5,
        backgroundColor: '#fff',
        borderColor: '#afafaf',
        
    },
    footer:{
        marginTop: height * -0.35,
        backgroundColor: '#fff',
        borderRadius: 30,
        paddingBottom: height * 0.05,
    },
    inputContent:{
        paddingTop: height * 0.01,
    },
    imageView:{
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageIcon:{
        width: width * 1,
        height: height * 1,
        resizeMode: 'cover',
    },
    askText:{
        marginTop: height * 0.025,
        marginLeft: width * 0.3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#fff',
        fontSize: 14,
    },
    mainText:{
        marginTop: height * 0.05,
        color: 'black',
        fontWeight: '600',
        fontSize:32,
        paddingLeft: width * 0.04,
    },
    buttonContent:{
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button1:{
        height: 53,
        margin: height * 0.01,
        fontSize: 18,
        padding: 12,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        margin: 11,
        elevation: 3,
        backgroundColor: colors.primary,
    },
    button2:{
        height: 53,
        fontSize: 18,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        margin: 11,
        elevation: 3,
        marginTop: height * -0.01,
        backgroundColor: 'white',
        borderColor:colors.primary,
        borderWidth: 2,
        borderRadius: 6,
        marginBottom: 40,

    },
    loginText:{
        fontSize: 18,
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: '#fff',
    },
    registerText:{
        fontSize: 18,
        textAlign: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: colors.primary
    }
});
