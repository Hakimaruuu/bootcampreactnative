import React, { useEffect, useState } from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity, StyleSheet, TextInput, FlatList, Dimensions, Pressable, Alert,} from 'react-native'
import { StatusBar } from 'expo-status-bar';

const {height,width} = Dimensions.get('window');


export default function Home({route, navigation}) {
    const { username } = route.params;
    const [items, setItems]=useState([]);
    const [items1, setItems1]=useState([]);
    const [items2, setItems2]=useState([]);
    const [items3, setItems3]=useState([]);
    const [items4, setItems4]=useState([]);
    
    const actionOnRow=(item)=>{
        console.log('selected item :',item)
        navigation.navigate('Details', {
            Data: item,
        })
    }

    const settings=(item)=>{
        console.log('setting data :',item)
        navigation.navigate('Settings', {
            Data: item,
        })
    }

    const GetData=()=>{
        fetch('https://fakestoreapi.com/products')
            .then(res=>res.json())
            .then(json=>setItems(json))
            console.log(items)
    }

    useEffect(()=> {
        GetData()
    },[])

    return (
        <>
        <StatusBar style='light' translucent={true} backgroundColor='#21bfa8'/>
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image source={require('../asset/header.bmp')} style={styles.gambarHeader} />
                </View>
                <View style={styles.content2}>
                    <Image source={require('../asset/Saldo.png')} style={{}} />
                    <Image source={require('../asset/Frame11.png')} style={{marginTop: 20, paddingTop: 5, width: width * 0.98}} />
                </View>
                <Image source={require('../asset/supersale.png')} style={{width: width, height: height * 0.15, marginTop: -30}} />
                <View>
                    <Text style={{paddingLeft: 15, fontWeight: 'bold', marginTop: 10, fontSize: 20, marginBottom: 4,}}>
                        Items Might You Like
                    </Text>
                    <FlatList
                        data={items}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={items => items.id.toString()}
                        style={{width: width + 5, height: height * 0.2}}
                        renderItem={({item})=>{
                            return (
                                <View style={{}}>
                                    {Math.random() < 0.2 ? 
                                    <View style={styles.content3}>
                                        <TouchableOpacity onPress={ () => actionOnRow(item)}>
                                            <Image source={{uri: item.image}} style={styles.gambar} />
                                            <Text style={styles.contentText3}>
                                                {item.title}
                                            </Text>
                                            <Text style={styles.contentText4}>
                                                Rp. {item.price*14000},-
                                            </Text>
                                        </TouchableOpacity>
                                    </View> : null }
                                </View>
                            )
                        }
                    }
                    />
                </View>
                <View>
                    <Text style={{paddingLeft: 15, fontWeight: 'bold', marginTop: 10, fontSize: 20, marginBottom: 4,}}>
                        By Category
                    </Text>
                    <Text style={{paddingLeft: 15, fontWeight: 'bold', marginTop: 10, fontSize: 16, marginBottom: 4,}}>
                        Men's Clothing
                    </Text>
                    <FlatList
                        data={items}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={items => items.id.toString()}
                        style={{width: width + 5, height: height * 0.2}}
                        renderItem={({item})=>{
                            return (
                                <View style={{}}>
                                    {item.category == "men's clothing" ? 
                                    <View style={styles.content3}>
                                        <TouchableOpacity onPress={ () => actionOnRow(item)}>
                                            <Image source={{uri: item.image}} style={styles.gambar} />
                                            <Text style={styles.contentText3}>
                                                {item.title}
                                            </Text>
                                            <Text style={styles.contentText4}>
                                                Rp. {item.price*14000},-
                                            </Text>
                                        </TouchableOpacity>
                                    </View> : null}
                                </View>
                            )
                        }
                    }
                    />
                </View>
                <View>
                    
                    <Text style={{paddingLeft: 15, fontWeight: 'bold', marginTop: 10, fontSize: 16, marginBottom: 4,}}>
                        Electronics
                    </Text>
                    <FlatList
                        data={items}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={items => items.id.toString()}
                        style={{width: width + 5, height: height * 0.2}}
                        renderItem={({item})=>{
                            return (
                                <View style={{}}>
                                    {item.category == "electronics" ? 
                                    <View style={styles.content3}>
                                        <TouchableOpacity onPress={ () => actionOnRow(item)}>
                                            <Image source={{uri: item.image}} style={styles.gambar} />
                                            <Text style={styles.contentText3}>
                                                {item.title}
                                            </Text>
                                            <Text style={styles.contentText4}>
                                                Rp. {item.price*14000},-
                                            </Text>
                                        </TouchableOpacity>
                                    </View> : null}
                                </View>
                            )
                        }
                    }
                    />
                </View>
                <View>
                    
                    <Text style={{paddingLeft: 15, fontWeight: 'bold', marginTop: 10, fontSize: 16, marginBottom: 4,}}>
                        Jewelery
                    </Text>
                    <FlatList
                        data={items}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={items => items.id.toString()}
                        style={{width: width + 5, height: height * 0.2}}
                        renderItem={({item})=>{
                            return (
                                <View style={{}}>
                                    {item.category == "jewelery" ? 
                                    <View style={styles.content3}>
                                        <TouchableOpacity onPress={ () => actionOnRow(item)}>
                                            <Image source={{uri: item.image}} style={styles.gambar} />
                                            <Text style={styles.contentText3}>
                                                {item.title}
                                            </Text>
                                            <Text style={styles.contentText4}>
                                                Rp. {item.price*14000},-
                                            </Text>
                                        </TouchableOpacity>
                                    </View> : null}
                                </View>
                            )
                        }
                    }
                    />
                </View>
            </View>
        </ScrollView>
        <View style={styles.footer}>
            <View style={styles.content1}>
                <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'space-between',}}>
                    <Pressable style={{}} onPress={{}}>
                        <Image source={require('../asset/TabbarB1.png')} style={{}} />
                    </Pressable>
                    <Pressable style={{}}>
                        <Image source={require('../asset/Tabbar2.png')} style={{}} />
                    </Pressable>
                    <Pressable style={{}}>
                        <Image source={require('../asset/Tabbar3.png')} style={{}} />
                    </Pressable>
                    <Pressable style={{}}>
                        <Image source={require('../asset/Tabbar4.png')} style={{}} />
                    </Pressable>
                    <Pressable style={{}} onPress={()=>settings(username)}>
                        <Image source={require('../asset/Tabbar5.png')} style={{}} />
                    </Pressable>
                </TouchableOpacity>
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        paddingBottom: 120,
    },
    gambarHeader:{
        width: width,
        height: height * 0.25,
    },
    gambar:{
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 50,
        width: 75,
        height: 75,
        borderRadius: 10,
        borderWidth: 3,
        resizeMode: 'center',
    },
    header:{
        paddingTop: 30,
        paddingHorizontal: 16,
        paddingBottom: 3,
        backgroundColor: '#21bfa8',
        alignItems: 'center',
    },
    title:{
        color: '#fff',
        fontSize: 20,
    },
    content1:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
        paddingHorizontal: 16,
    },
    content2:{
        paddingTop: height * 0.02,
        height: height * 0.25,
        paddingHorizontal: 16,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        backgroundColor: '#255ba5',
    },
    contentInner2:{
        marginTop: height * 0.1,
        padding: 10,
        borderRadius: 15,
        color: '#000',
        backgroundColor: '#fff',

    },
    content3:{
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 6,
        padding: 10,
        elevation: 3,
        borderRadius: 15,
        width: 200,
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentInner3:{
        
    },
    contentText3:{
        fontSize: 12,
        flexShrink: 1,
    },
    contentText4:{
        fontSize: 12,
        marginTop: 4,        
    },
    input:{
        borderWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10,
        borderColor:'#fff',
        color: '#fff',
    },
    footer:{
        flex: 1,
        position: 'absolute',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        width: width,
        backgroundColor: '#fff',
        elevation: 10,
        marginTop: height * 0.9,
        borderRadius: 15,
        paddingBottom: 100,
    }
});
