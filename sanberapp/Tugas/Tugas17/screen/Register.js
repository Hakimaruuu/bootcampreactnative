import { StatusBar } from 'expo-status-bar';
import React from 'react'
import {
    View,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Pressable,
} from 'react-native';
import firebase from 'firebase';

const {height,width} = Dimensions.get('window');

const colors = {
    primary: '#51a5f2',
    secondary: '#013064',
    background: '#212c3a',
    black: '#212121'

};

export default function Register({navigation}) {
    const [email,setEmail] = React.useState('');
    const [name,setName] = React.useState('');
    const [password,setPassword] = React.useState('');
    const [cpassword,setCPassword] = React.useState('');
    const [isError,setIsError] = React.useState(false);

    
    const submit = () =>{
        if (password == cpassword){
            firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then((userCredential)=>{
                var user = userCredential.user;
                alert('Registered Successfully');
                navigation.navigate('Login');
            })
            .catch ((error)=>{
                const errorCode = error.code;
                const errorMessage = error.message;
            });
        } else {
            setIsError(true);
        }
        
    }

    return (
        <>
        <StatusBar style='light' translucent={false} backgroundColor={'#51a5f2'}/>
        <View style={styles.container}>
        <View style={styles.imageView}>
                <Image source={require('../asset/bgc.jpg')} style={styles.imageIcon} />
            </View>
            <View style={styles.mainContent}>
                <View style={styles.footer}>
                <Text style={styles.mainText}>
                    Let's Start
                </Text>
                <TouchableOpacity style={styles.inputContent}>
                    <TextInput style={styles.inputText} value={name} placeholder="Name" onChangeText={(value)=>setName(value)} />
                    <TextInput style={styles.inputText} value={email} placeholder="Email" onChangeText={(value)=>setEmail(value)} />
                    <TextInput style={styles.inputText} secureTextEntry={true} value={password} placeholder="Password" onChangeText={(value)=>setPassword(value)} />
                    <TextInput style={styles.inputText} secureTextEntry={true} value={cpassword} placeholder="Confirm Password" onChangeText={(value)=>setCPassword(value)} />
                    <Pressable style={styles.button1} onPress={submit}>
                        <Text style={styles.loginText}>
                            REGISTER
                        </Text>
                    </Pressable>
                </TouchableOpacity>
                <Text style={styles.askText}>
                    Already have an account?
                </Text>
                <TouchableOpacity style={styles.buttonContent}>
                    <Pressable style={styles.button2} onPress={()=>navigation.navigate('Login')}>
                        <Text style={styles.registerText}>
                            Sign In
                        </Text>
                    </Pressable>
                </TouchableOpacity>
                </View>
            </View>
        </View>
        </>
    );
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: colors.background,
        flex: 1,
    },
    footer:{
        marginTop: height * -0.65,
        backgroundColor: '#fff',
        borderRadius: 30,
        paddingBottom: height * 0.1,
    },
    askText:{
        marginTop: height * 0.025,
        marginLeft: width * 0.3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#000',
        fontSize: 14,
    },
    inputText:{
        height: 53,
        margin: height * 0.01,
        borderWidth: 1,
        fontSize: 18,
        padding: 12,
        color: '#afafaf',
        borderRadius: 5,
        backgroundColor: '#fff',
        borderColor: '#afafaf',
        
    },
    inputContent:{
        paddingTop: height * 0.01,
    },
    imageView:{
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageIcon:{
        width: width * 1,
        height: height * 1,
        resizeMode: 'cover',
    },
    mainText:{
        marginTop: height * 0.05,
        color: 'black',
        fontWeight: '600',
        fontSize:36,
        color: '#000',
        paddingLeft: width * 0.04,
    },
    mainContent:{
        marginTop: height * -0.08,
    },
    buttonContent:{
        height: height * 0.05,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button1:{
        height: 53,
        margin: height * 0.01,
        fontSize: 18,
        padding: 12,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        margin: 11,
        elevation: 3,
        backgroundColor: colors.primary,
    },
    button2:{
        height: 30,
        width: 128,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 11,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'white',
        borderColor:colors.primary,
        borderWidth: 2,

    },
    loginText:{
        fontSize: 18,
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: '#fff',
    },
    registerText:{
        fontSize: 14,
        textAlign: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: colors.primary
    }
});
