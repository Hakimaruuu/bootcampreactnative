import React from 'react'
import { View, Text, Image, Button, TouchableOpacity, StyleSheet, ScrollView, Dimensions, Pressable,} from 'react-native'
import { StatusBar } from 'expo-status-bar';

const {height,width} = Dimensions.get('window');

const colors = {
    primary: '#51a5f2',
    secondary: '#013064',
    background: '#212c3a',
    black: '#212121'

};

export default function Settings({route, navigation}) {
    const { Data } = route.params;

    return (
        <>
        <StatusBar style='dark' translucent={true} />
        <View style={styles.container}>
            <View style={{elevation: 10}}>
                <Image source={require('../asset/sb.png')} style={{marginTop: height * 0.1,}} />
            </View>
            <ScrollView>
                <View style={{backgroundColor: '#f2f2f2', paddingBottom: 100,}}>
                    <Text style={{justifyContent: 'center', alignItems: 'center',flexDirection: 'row', paddingLeft: width * 0.025, paddingTop: 20, fontSize: 22, fontWeight: 'bold',}}>
                        {Data}
                    </Text>
                    <Image source={require('../asset/g9.png')} style={{marginTop: height * 0.05,}} />
                    <Image source={require('../asset/g12.png')} style={{marginTop: height * 0.05,}} />
                </View>
            </ScrollView>
            <View style={styles.footer}>
                <View style={styles.content1}>
                    <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'space-between',}}>
                        <Pressable style={{}} onPress={()=>navigation.navigate('Home', {username: Data})}>
                            <Image source={require('../asset/tabbar1off.png')} style={{}} />
                        </Pressable>
                        <Pressable style={{}}>
                            <Image source={require('../asset/Tabbar2.png')} style={{}} />
                        </Pressable>
                        <Pressable style={{}}>
                            <Image source={require('../asset/Tabbar3.png')} style={{}} />
                        </Pressable>
                        <Pressable style={{}}>
                            <Image source={require('../asset/Tabbar4.png')} style={{}} />
                        </Pressable>
                        <Pressable style={{}}>
                            <Image source={require('../asset/tabbar5on.png')} style={{}} />
                        </Pressable>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
    },
    gambarHeader:{
        width: width,
        height: height * 0.25,
    },
    button1:{
        paddingLeft: 10,
        paddingRight: 10,
    },
    gambar:{
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 10,
        width: width,
        height: height * 0.3,
        resizeMode: 'contain',
    },
    gambarStar:{
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: width * 0.01,
        width: width * 0.06,
        height: height * 0.06,
        borderRadius: 10,
        resizeMode: 'contain',
    },
    header:{
        paddingTop: 90,
        paddingHorizontal: 16,
        paddingBottom: 25,
        backgroundColor: '#fff',
        alignItems: 'center',
        elevation: 5
    },
    title:{
        color: '#fff',
        fontSize: 20,
    },
    content1:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
        paddingHorizontal: 16,
    },
    content2:{
        paddingTop: height * 0.02,
        height: height * 0.25,
        paddingHorizontal: 16,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        backgroundColor: '#255ba5',
    },
    contentInner2:{
        marginTop: height * 0.1,
        padding: 10,
        borderRadius: 15,
        color: '#000',
        backgroundColor: '#fff',

    },
    content3:{
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 6,
        padding: 10,
        elevation: 10,
        borderRadius: 15,
        width: 200,
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentInner3:{
    },
    contentText3:{
        fontSize: 12,
        flex: 1,
        flexShrink: 1,
    },
    input:{
        borderWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10,
        borderColor:'#fff',
        color: '#fff',
    },
    footer:{
        flex: 1,
        position: 'absolute',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        width: width,
        backgroundColor: '#fff',
        elevation: 10,
        marginTop: height * 0.9,
        borderRadius: 15,
        paddingBottom: 100,
    }
});
