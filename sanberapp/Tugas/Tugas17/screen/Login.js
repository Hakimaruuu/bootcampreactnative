import { StatusBar } from 'expo-status-bar';
import React from 'react'
import {
    View,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Pressable,
} from 'react-native';
import firebase from 'firebase';

const {height,width} = Dimensions.get('window');

const colors = {
    primary: '#51a5f2',
    secondary: '#013064',
    background: '#212c3a',
    black: '#212121'

};

export default function Login({navigation}) {
    const [email,setEmail] = React.useState('');
    const [password,setPassword] = React.useState('');
    const [isError,setIsError] = React.useState(false);

    

    const submit = () =>{
        firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then((userCredential)=>{
            var user = userCredential.user;
            alert('Login Successfully');
            navigation.navigate('Home', {
                username: user.email,
            });
            console.log(username);
        })
        .catch ((error)=>{
            const errorCode = error.code;
            const errorMessage = error.message;
        });
    }

    return (
        <>
        <StatusBar style='light' translucent={false} backgroundColor={'#51a5f2'}/>
        <View style={styles.container}>
            <View style={styles.imageView}>
                <Image source={require('../asset/bgc.jpg')} style={styles.imageIcon} />
            </View>
            <Text style={styles.greeting}>
                Welcome back
            </Text>
            <View style={styles.mainContent}>
                <View style={styles.footer}>
                    <Text style={styles.mainText}>
                        Sign In
                    </Text>
                    <TouchableOpacity style={styles.inputContent}>
                        <TextInput style={styles.inputText} value={email} placeholder="Email" onChangeText={(value)=>setEmail(value)} />
                        <TextInput style={styles.inputText} value={password} secureTextEntry={true} placeholder="Password" onChangeText={(value)=>setPassword(value)} />
                        <Pressable style={styles.button1} onPress={submit}>
                            <Text style={styles.loginText}>
                                LOGIN
                            </Text>
                        </Pressable>
                    </TouchableOpacity>
                    <Text style={styles.askText}>
                        Don't have an account?
                    </Text>
                    <TouchableOpacity style={styles.buttonContent}>
                        <Pressable style={styles.button2} onPress={()=>navigation.navigate('Register')}>
                            <Text style={styles.registerText}>
                                REGISTER NOW
                            </Text>
                        </Pressable>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
        </>
    );
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: colors.background,
        flex: 1,
    },
    footer:{
        marginTop: height * -0.7,
        backgroundColor: '#fff',
        borderRadius: 30,
        paddingBottom: height * 0.05,
    },
    inputText:{
        height: 53,
        margin: height * 0.01,
        borderWidth: 1,
        fontSize: 18,
        padding: 12,
        color: '#afafaf',
        borderRadius: 5,
        backgroundColor: '#fff',
        borderColor: '#afafaf',
        
    },
    inputContent:{
        paddingTop: height * 0.01,
    },
    imageView:{
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageIcon:{
        width: width * 1,
        height: height * 1,
        resizeMode: 'cover',
    },
    askText:{
        marginTop: height * 0.025,
        marginLeft: width * 0.31,
        flexDirection: 'row',
        color: '#000',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 14,
    },
    greeting:{
        fontWeight: '600',
        fontSize:36,
        color: '#fff',
        paddingLeft: width * 0.04,
    },
    mainText:{
        marginTop: height * 0.05,
        fontWeight: '600',
        fontSize:36,
        color: '#000',
        paddingLeft: width * 0.04,
    },
    mainContent:{
        height: height * -0.01,
    },
    buttonContent:{
        height: height * 0.05,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button1:{
        height: 53,
        margin: height * 0.01,
        fontSize: 18,
        padding: 12,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        margin: 11,
        elevation: 3,
        backgroundColor: colors.primary,
    },
    button2:{
        height: 30,
        width: 128,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 11,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'white',
        borderColor:colors.primary,
        borderWidth: 2,
        borderRadius: 6,

    },
    loginText:{
        fontSize: 18,
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: '#fff',
    },
    registerText:{
        fontSize: 14,
        textAlign: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: colors.primary
    }
});
