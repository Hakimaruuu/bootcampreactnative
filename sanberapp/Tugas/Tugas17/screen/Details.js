import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView, Dimensions, Pressable,} from 'react-native'
import { StatusBar } from 'expo-status-bar';

const {height,width} = Dimensions.get('window');

const colors = {
    primary: '#51a5f2',
    secondary: '#013064',
    background: '#212c3a',
    black: '#212121'

};

export default function Details({route, navigation}) {
    const { Data } = route.params;

    return (
        <>
        <StatusBar style='dark' translucent={true} />
        <View style={styles.container}>
            <View style={styles.header}>
                <Image source={{uri: Data.image}} style={styles.gambar} />
            </View>
            <ScrollView>
                <View style={{borderRadius: 25,}}>
                    <Text style={{paddingTop:height * 0.025, paddingLeft: width * 0.025, paddingRight: 30,fontWeight: 'bold', fontSize:18,}}>
                        {Data.title}
                    </Text>
                    <Text style={{fontSize: 24, paddingLeft: width * 0.025, color: colors.primary, fontWeight: 'bold', marginBottom: 15,}}>
                        Rp. {Data.price * 14000},-
                    </Text>
                    <View style={{borderColor:'#afafaf', borderTopWidth: 0.5, flexDirection: 'row', alignItems: 'center', paddingLeft: width * 0.025,}}>
                        <Image source={require('../asset/star.png')} style={styles.gambarStar} />
                        <Text style={{fontSize: 16, paddingLeft: width * 0.025, color: '#000',}}>
                            {Data.rating.rate}/ 5.0 | {Data.rating.count} sold
                        </Text>
                    </View>
                </View>
                <View style={{backgroundColor: '#f2f2f2', elevation: 0, marginTop: 4, paddingBottom: height * 0.125,}}>
                    <Text style={{paddingLeft: 20, paddingTop: 10, paddingBottom: 5,}}>
                        Category 
                    </Text>
                    <Text style={{paddingLeft: 20, color: colors.primary,}}>
                        {Data.category}
                    </Text>
                    <Text style={{paddingLeft: 20, paddingTop: 5,}}>
                        {Data.description}
                    </Text>
                </View>
            </ScrollView>

            <View style={styles.footer}>
                <View style={styles.content1}>
                    <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'space-evenly', padding: 10,}}>
                        <Pressable style={styles.button1} onPress={{}}>
                            <Image source={require('../asset/chat.png')} style={{}} />
                        </Pressable>
                        <Pressable style={styles.button1}>
                            <Image source={require('../asset/beli.png')} style={{}} />
                        </Pressable>
                        <Pressable style={styles.button1}>
                            <Image source={require('../asset/cart.png')} style={{}} />
                        </Pressable>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
    },
    gambarHeader:{
        width: width,
        height: height * 0.25,
    },
    button1:{
        paddingLeft: 10,
        paddingRight: 10,
    },
    gambar:{
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 10,
        width: width,
        height: height * 0.3,
        resizeMode: 'contain',
    },
    gambarStar:{
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: width * 0.01,
        width: width * 0.06,
        height: height * 0.06,
        borderRadius: 10,
        resizeMode: 'contain',
    },
    header:{
        paddingTop: 90,
        paddingHorizontal: 16,
        paddingBottom: 25,
        backgroundColor: '#fff',
        alignItems: 'center',
        elevation: 5
    },
    title:{
        color: '#fff',
        fontSize: 20,
    },
    content1:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 1,
    },
    content2:{
        paddingTop: height * 0.02,
        height: height * 0.25,
        paddingHorizontal: 16,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        backgroundColor: '#255ba5',
    },
    contentInner2:{
        marginTop: height * 0.1,
        padding: 10,
        borderRadius: 15,
        color: '#000',
        backgroundColor: '#fff',

    },
    content3:{
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 6,
        padding: 10,
        elevation: 10,
        borderRadius: 15,
        width: 200,
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentInner3:{
    },
    contentText3:{
        fontSize: 12,
        flex: 1,
        flexShrink: 1,
    },
    input:{
        borderWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10,
        borderColor:'#fff',
        color: '#fff',
    },
    footer:{
        flex: 1,
        position: 'absolute',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 10,
        marginTop: height * 0.9,
        borderRadius: 15,
        marginHorizontal: 10,
    }
});
