// NO 1
// RELEASE 0
class Animal {
    constructor(name,legs,cold_blooded){
        this.name = name;
        this.legs = legs;
        this.cold_blooded = false;
    }
}

const sheep = new Animal("shaun", 4);

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// RELEASE 1
class Ape extends Animal{
    constructor(name){
        super(name);
        this.legs = 2;
        this.cold_blooded = false;
    }

    yell(){
        console.log('Auooo');
    }
}

class Frog extends Animal{
    constructor(name){
        super(name)
        this.legs = 4;
        this.cold_blooded = true;
    }

    jump(){
        console.log('hop hop');
    }
}

const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"

const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"

// NO 2
class Clock {
    constructor({template}) {
        this.timer;
        this.template = template;
    }

    render(template){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    start(){
        this.render(this.template);
        this.timer = setInterval(()=>this.render(this.template),1000)
    }

    stop(){
        clearInterval(timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

// 23:05:08
// 23:05:09
// 23:05:10
// 23:05:11
// 23:05:12
// 23:05:13
// 23:05:14
// 23:05:15
// 23:05:16
// 23:05:17
// 23:05:18
