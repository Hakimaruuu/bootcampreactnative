// if-else

var nama = "Jenita";
var peran = "Guard";


// output untuk input nama = '' dan peran = ''
if (nama.length == 0){
    console.log('Nama harus diisi!');
// output untuk nama = 'john' dan peran = ''
} else if (nama.length>0 && peran.length==0){
    console.log('Halo '+nama+', silahkan pilih peranmu!');
// output untuk input nama = 'jane' dan peran = 'penyihir'
} else if (nama == 'Jane' && peran == 'Penyihir'){
    console.log('Selamat datang di Dunia Werewolf, Jane!');
    console.log('Halo penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!')
    // output untuk input nama = 'junaedi' dan peran = 'werewolf'
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
    console.log('Selamat datang di Dunia Werewolf, Junaedi!');
    console.log('Halo Werewolf Junaedi, kamu akan memakan mangsa setiap malam!');
// output untuk input nama = 'jenita' dan peran = 'guard'
} else if (nama == 'Jenita' && peran == 'Guard') {
    console.log('Selamat datang di Dunia Werewolf, Jenita!');
    console.log('Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf!');
}

console.log('\n');

// switch-case

var hari = 15;
var bulan = 9;
var tahun = 2021;

if ((hari > 0 && hari <= 31)&&(tahun > 1900 && tahun <= 2200)){
    switch(bulan){
        case 1: {console.log(hari+' Januari '+tahun); break;}
        case 2: {console.log(hari+' Januari '+tahun); break;}
        case 3: {console.log(hari+' Januari '+tahun); break;}
        case 4: {console.log(hari+' Januari '+tahun); break;}
        case 6: {console.log(hari+' Juni '+tahun); break;}
        case 5: {console.log(hari+' Mei '+tahun); break;}
        case 7: {console.log(hari+' Juli '+tahun); break;}
        case 8: {console.log(hari+' Agustus '+tahun); break;}
        case 9: {console.log(hari+' September '+tahun); break;}
        case 10: {console.log(hari+' Oktober '+tahun); break;}
        case 11: {console.log(hari+' November '+tahun); break;}
        case 12: {console.log(hari+' Desember '+tahun); break;}
        default: {console.log('Bulan salah!'); break;}
    }
}