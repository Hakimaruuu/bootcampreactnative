// NO 1
// LOOPING PERTAMA
console.log('LOOPING PERTAMA');
var f = 2;
while(f<=20){
    console.log(f,'- I love coding');
    f+=2;
}

// LOOPING KEDUA
console.log('LOOPING KEDUA');
var g = 20;
while(g>0){
    console.log(g,'- I will become a mobile developer');
    g-=2;
}
console.log('')

// NO 2
for (let x = 1; x <= 20; x++) {
    if(x % 2 == 0){
        console.log(x,'- Berkualitas');
    } else if (x % 3 == 0){
        console.log(x,'- I Love Coding');
    } else if (x % 2 == 1){
        console.log(x,'- Santai');
    }
    
}
console.log('')

// NO 3
let p = 8;
let l = 4;
for (let i = 0; i < l; i++) {
    var s = '';
    for (let j = 0; j < p; j++) {
        s+='#';
    }
    console.log(s);
}
console.log('');

// NO 4
var string = '';
for (let i=0; i < 7; i++){
    console.log(string+='#')
}
console.log('')

// NO 5
var m = 0;
var n = 0;
while (m < 8){
    var string = '';
    if (m % 2 == 0){
        for (let i=0; i < 4; i++){
            string+=' #';
        }
        console.log(string);
    } else {
        for (let i=0; i < 4; i++){
            string+='# ';
        }
        console.log(string);
    }
    m++;
}
console.log('')
