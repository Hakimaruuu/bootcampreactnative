/* NO 1 */
function arrayToObject(arr) {
    var now = new Date();
    var thisYear = now.getFullYear() //2021
    var x = 0
    if (arr.length > 0){
        while (x < arr.length){
            var f = arr[x][0]; var l = arr[x][1];
            var g = arr[x][2]; var a = arr[x][3];
    
            if ((!a)||(thisYear - a < 0)){
                a = 'Invalid Birth Year';
            } else {
                a = thisYear - a;
            }
    
            var arrObj = {}
            var objDet = {
                firstName: f, 
                lastName: l, 
                gender: g, 
                age: a
            }

            var str = (x+1)+'.'+arr[x][0]+' '+arr[x][1]+':';
            arrObj[str] = objDet;
            console.log(arrObj);
            x++;
        }
    } else {
        var arrObj = {};
        console.log(arrObj);
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

// Error case 
arrayToObject([]) // "{}"


/* NO 2 */
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(memberId){
        if (money > 50000){
            var listItem = {
                stacattu : 1500000,
                zoro : 500000,
                hn : 250000,
                uniklooh : 175000,
                casing : 50000
            }
            if (memberId == '1820RzKrnWn08'){
                var listPurchased = [
                    'Sepatu Stacattu',
                    'Baju Zoro',
                    'Baju H&N',
                    'Sweater Uniklooh',
                    'Casing Handphone'
                ];
                var changeMoney = money - (listItem.stacattu + listItem.zoro + listItem.hn + listItem.uniklooh + listItem.casing)
                var mem = {
                    memberId : memberId,
                    money : money,
                    listPurchased : listPurchased,
                    changeMoney : changeMoney
                }
                console.log(mem);        
            } else if (memberId == '82Ku8Ma742'){
                var listPurchased = [
                    'Casing Handphone'
                ];
                var changeMoney = money - listItem.casing
                var mem = {
                    memberId : memberId,
                    money : money,
                    listPurchased : listPurchased,
                    changeMoney : changeMoney
                }
                console.log(mem);
            }
        } else {
            console.log('Mohon maaf, uang tidak cukup!');
        }
    } else {
        console.log('Mohon maaf, Toko X hanya berlaku untuk member saja!');
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

/* NO 3 */
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var a = [];
    var counting = false;
    for (let i = 0; i < arrPenumpang.length; i++) {
        var cost = 0;
        for (let j = 0; j < rute.length; j++) {
            if (arrPenumpang[i][1] == rute[j]){
                counting = true;
            } else if (arrPenumpang[i][2] == rute[j]){
                counting = false;
            }

            if (counting){
                cost+=2000;
            }
        }
        var psgr = { penumpang: arrPenumpang[i][0], naikDari: arrPenumpang[i][1], tujuan: arrPenumpang[i][2], bayar: cost };
        a.push(psgr);
    }
    console.log(a);
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

// output undefined dari output console.log